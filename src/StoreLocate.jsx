import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
// import data
import configData from "./data/config.json";

export default function StoreLocate({ mode, store, sendStoreID }) {
    const navigate = useNavigate();
    const [storeID, setStoreID] = useState(store);
    // extract store list
    const storeList = configData[mode]["NAME"];

    // update storeID
    useEffect(() => {
        setStoreID(store);
    }, [store]);

    // function to select store
    function selectStore(id) {
        setStoreID(id);
        sendStoreID(id);
        navigate("/orders")
    };

    
    let storeDivs = Object.keys(storeList).map((k) => {
        // do not show dev store in prod mode
        if (mode === "prod" && k === "dev") {
            return;
        } else if (k === storeID) {
            return (
                <div id={k} className="storeDiv bgOrange" onClick={() => selectStore(k)}>
                    <p className="textMed bold textWhite">{storeList[k]}</p>
                </div>
            )
        } else {
            return (
                <div id={k} className="storeDiv" onClick={() => selectStore(k)}>
                    <p className="textMed bold textWhite">{storeList[k]}</p>
                </div>
            )
        };
    });


    return (
        <div className="storeContainer">
             <p className="textLarge textWhite bold textLeft spacingTop3 spacing2">Select your store location</p>
            <div className="storeGrid">
                { storeDivs }
            </div>
        </div>
    )
};