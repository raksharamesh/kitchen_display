import React, { useState, useEffect } from 'react';
import io from 'socket.io-client';
// import data
import configData from "./data/config.json";
import ding from "./audio/ding.mp3"


export default function Websocket({ mode, store, sendData, removeFlag, sendRemoveFlag, removeOrder, refresh, sendRefresh }) {
    // define order list
    const [allItemData, setAllItemData] = useState([]); 
    // config variables
    const [STORE, setSTORE] = useState(store);
    // const [websocketURL, setWebsocketURL] = useState("https://avapay2.graphen.ai/square/webhook");
    const [locationID, setLocationID] = useState(configData[mode]["LOCATION_ID"][store]);
    // define sound
    const dingSound = new Audio(ding);
    let playSound = false; // sound flag


    useEffect(() => {
        setLocationID(configData[mode]["LOCATION_ID"][store]);
        setSTORE(store)
    }, [mode, store]);

    // pop order from allItemData and reset flag
    useEffect(() => {
        if (removeFlag) {
            if (removeOrder !== undefined && Object.keys(removeOrder).length !== 0) {
                // pop order from list
                console.log("REMOVING: ", removeOrder.order_number)
                popItemData(removeOrder);
                // reset flag
                sendRemoveFlag(false);
            };
        };
    }, [removeFlag, removeOrder]);

    // send data to parent
    useEffect(() => {
        sendData(allItemData);
    }, [allItemData]);


    /////////////////////////////////
    // FUNCTION: COMPILE ITEM DATA //
    /////////////////////////////////
    // append item data to allItemData
    function appendItemData(data) {
        // check if item data already exists in allItemData
        popItemData(data);
        // append item data to allItemData
        setAllItemData(currData => [...currData, data]);

        // only play sound for new order
        if (removeFlag === false && playSound === true) {
            dingSound.play();
        };
    };

    // function to match dictionaries based on 2 parameters (order_id and location_id)
    function matchOrderItem(dict, id, location) {
        let match = dict.order_id === id && dict.square_location_id === location;
        if (match) {
            playSound = false;
            console.log("DO NOT PLAY SOUND")
        };
        return dict.order_id === id && dict.square_location_id === location;
    };
    // pop item data from allItemData
    function popItemData(data) {
        let orderID = data.order_id;
        let locID = data.square_location_id;

        // filter out the item data that matches the order number and item number
        setAllItemData((allOrders) => {
            return allOrders.filter(o => !matchOrderItem(o, orderID, locID));
        });
    };


    //////////////////////////////////
    // CONNECT TO WEBSOCKET SERVER //
    /////////////////////////////////
    const websocketURL = configData[mode]["APP_HOME"] + "square/webhook";

    // clean up the socket connection when the component unmounts
    useEffect(() => {
        const socket = io.connect(websocketURL, { debug: true });
        return () => {
            console.log("DISCONNECTED on rerender");
            socket.disconnect();
        };
    }, []);

    useEffect(() => {
        const socket = io.connect(websocketURL, { debug: true });
        socket.on('connect', () => {
            console.log("Connected to Socket.IO server! ROOM: ", locationID);
            socket.emit("join", { room: locationID });
            const nickname = configData[mode]["NICKNAME"];
            console.log("SOCKET URL: ", websocketURL, nickname)
            socket.emit("set_nickname", { nickname: nickname, room_id: locationID }); //test

        });

        // recieve data
        socket.on('kds_order_create', (data, acknowledge) => {
            console.log("NEW ORDER: ", data);
            if (data !== undefined) {
                // send acknowledgement
                if (acknowledge) {
                    acknowledge({ "client_msg": "kds updated", "order_id": data.order_id, "order_number": data.order_number })
                };

                // play sound and append new order 
                // dingSound.play();
                playSound = true;
                appendItemData(data); 
            };
        });

        // disconnect when new store is selected
        return () => {
            console.log("DISCONNECTED on store change");
            socket.disconnect();
        };
    }, [STORE]);

    ///////////////////////////////
    // PULL ORDERS UPON REFRESH //
    //////////////////////////////
    useEffect(() => {
        // if refreshed pull orders from server
        if (refresh) {
            // socket.on("", (data) => {
            //     socket.emit("", { room: locationID });
            // });
        };

    }, [refresh])


};