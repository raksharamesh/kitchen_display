import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faUtensils, faBan, faCircleXmark } from '@fortawesome/free-solid-svg-icons'
// import data
import configData from "./data/config.json";


// define source tag color
const sourceColor = {
    "KIOSK": "#FFB700",
    "WEB": "#52C1FF",
    "DOORDASH": "#FF7D05"
};

export default function OrderCards({ store, mode, orderData, sendRemove }) {
    const [locationID, setLocationId] = useState(configData[mode]["LOCATION_ID"][store]);
    const [baseURL, setBaseURL] = useState(configData[mode]["APP_HOME"]);
    const [strikeList, setStrikeList] = useState([]);
    // flag 
    const [showError, setShowError] = useState(false);


    useEffect(() => {
        setLocationId(configData[mode]["LOCATION_ID"][store]);
    }, [mode, store]);

    useEffect(() => {
        setBaseURL(configData[mode]["APP_HOME"]);
    }, [mode]);

    useEffect(() => {
        const container = document.getElementById("orderCardsContainer");
        // clear out all crossed out divs
        removeClass(container, "strikeThrough");
        removeClass(container, "bgFade");
        // add strikes out class back to items
        strikeList.map((i) => {
            addCrossOut(i);
        });
    }, [sendRemove]);

    // define timer
    let timer;
    // define timer for error popup
    React.useEffect(() => {
        if (showError) {
            clearTimeout(timer);
            timer = setTimeout(setShowError, 5000, false);
        } else {
            clearTimeout(timer);
        };
        return () => clearTimeout(timer);
    }, [showError]);


    ////////////////////////////////////////////
    // FUNCTION: MATCH ORDER NUM FOR REMOVAL //
    ///////////////////////////////////////////
    function matchOrderItem(o, orderStr) {
        // clean string so that only the order number remains 
        let itemID;
        if (o.length <= 5) {
            itemID = o.slice(0, -1);
        } else if (o.length > 5) {
            itemID = o.slice(0, -2);
        };
        let rmOrder = orderStr.replace("Order #", "");
        return rmOrder == itemID;
    }; 


    ///////////////////////////////
    // CREATE STORE NAME HEADER //
    //////////////////////////////
    let nameHeader = <div className="headerContainer">
                        <div className="floatLeft paddingLeft2">
                            <p className="subtitle textWhite ">Store: {configData[mode]["NAME"][store]}</p>
                        </div>
                    </div>


    //////////////////////////////
    // CREATE POST ERROR POPUP //
    /////////////////////////////
    let errorPopup = <div className="popupVeil" onClick={() => setShowError(false)}>
                        <div className="statusContainer">
                            <FontAwesomeIcon className="failIcon spacing5" icon={faCircleXmark} />
                            <p className="textMed bold">Unable to update order.</p>
                            <p className="textMed bold">Please Try Again</p>
                        </div>
                    </div>

    
    //////////////////////
    // FUNCTIONS: CARD //
    /////////////////////
    // convert to DateTime and return a time string
    function parseTime(timeStr, seconds) {
        // parse the string as Date object
        let date = new Date(timeStr);
        // extract the time components in EST
        let options;
        if (seconds) {
            options = { 
                hour: 'numeric', 
                minute: 'numeric', 
                second: 'numeric', 
                hour12: true, 
                timeZone: 'America/New_York' 
            };
        } else {
            options = { 
                hour: 'numeric', 
                minute: 'numeric', 
                hour12: true, 
                timeZone: 'America/New_York' 
            };
        };

        let timeStringEST = date.toLocaleTimeString('en-US', options);
        return timeStringEST
    };

    // cross out line items
    function crossOut(n, t, m) {
        let id = n.replace("num", "");

        let number = document.getElementById(n);
        let text = document.getElementById(t);
        let mod = document.getElementById(m);

        // toggle strikethrough 
        if (number.classList.contains("bgFade")) {
            number.classList.remove("bgFade");
            // remove striked item from list
            setStrikeList((idList) => {
                return idList.filter(i => i !== id);
            });
        } else {
            number.classList.add("bgFade");
            // add striked item to list
            setStrikeList(list => [...list, id]);
        };

        if (text.classList.contains("strikeThrough")) {
            text.classList.remove("strikeThrough")
        } else {
            text.classList.add("strikeThrough")
        };

        // get list of mod children and toggle strikethrough
        let modChildren = mod.children;
        for (let i = 0; i < modChildren.length; i++) {
            let child = modChildren[i];
            if (child.classList.contains("strikeThrough")) {
                child.classList.remove("strikeThrough")
            } else {
                child.classList.add("strikeThrough")
            };
        };
        
    };

    // iterate through all the children in a container and remove the identified class
    function removeClass(containerName, styleName) {
        // Remove the class if it exists
        if (containerName.classList.contains(styleName)) {
            containerName.classList.remove(styleName);
        };
    
        // Recursively traverse through child elements
        if (containerName.children.length > 0) {
            for (let i = 0; i < containerName.children.length; i++) {
                removeClass(containerName.children[i], styleName);
            }
        };
    };

    // add crossout for identified element
    function addCrossOut(id) {
        let n = id + "num";
        let t = id + "text";
        let m = id + "mod";
        let number = document.getElementById(n);
        let text = document.getElementById(t);
        let mod = document.getElementById(m);

        // catch error if item is not removed from strikeList
        try {
            // add styling 
            number.classList.add("bgFade");
            text.classList.add("strikeThrough");
            // get list of mod children and toggle strikethrough
            let modChildren = mod.children;
            for (let i = 0; i < modChildren.length; i++) {
                let child = modChildren[i];
                child.classList.add("strikeThrough");
            };
        } catch {
            return
        };
    };


    /////////////////
    // BACKEND API //
    /////////////////
    async function POSTdone(orderNum, orderStr, squareID, orderType, orderSource, entireOrder) {
        const postURL = baseURL + "square/webhook/kds"; // url based on config and login credentials

        // initialize data to send to clover backend
        let bumpedData = JSON.stringify({ 
            triggeredAction: "KDS Order Bumped",
            order_number: orderNum,
            order_id: orderStr, 
            square_order_id: squareID, 
            order_type: orderType,
            square_location_id: locationID, 
            order_name: "",
            source: orderSource 
        });

        fetch(postURL, {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
            },
            body: bumpedData
        }).then((res) => {
            console.log("POST Response pushing to DB ", res.statusText, res.status);

            // if api call goes through successfully
            if (res.status === 200) {
                // if successful pop the dictionary from orderData
                sendRemove(true, entireOrder);

                // remove ids from strikelist as well
                setStrikeList((idList) => {
                    return idList.filter(o => !matchOrderItem(o, orderNum));
                });
                // return res;
            } else {
                // try again
                setShowError(true);
                console.log("POST FAILED: try again");
            };

        // }).then((response) => {
        //     console.log("API response ", response);
        }).catch((err) => {
            console.log("Error ", err);
        });
    };


    /////////////////////////
    // CREATE ORDER CARDS //
    ////////////////////////
    // iterate through orderData list and display order cards
    let showOrders = orderData.map((o) => {
        if (o !== undefined && o !== null) {
            // only display card if location id matches
            let squareLocation = o["square_location_id"];
            if (squareLocation === locationID) {   

                try {
                    // extract order number, id, and square id
                    let order_num = o["order_number"]
                    let order_id = o["order_id"];
                    let square_order_id = o["square_order_id"];
                    // extract id
                    let ID = order_num.replace("Order #", "");
                    // extract upper data
                    let orderID = "#" + ID
                    let name = o["order_name"];
                    let orderTime = parseTime(o["order_time"], true);
                    let orderType = o["order_type"];
                    // extract lower data
                    let source = o["source"];
                    let boxColor = sourceColor[source];
                    let pickupTime = o["pickup_at"] !== "" ? parseTime(o["pickup_at"], false) : "";
                    let utensils = o["specialInstructions"];

                    // create utensils display
                    let showUtensils;
                    if (utensils !== "") {
                        if (utensils === "Add Utensils") {
                            showUtensils = <div className="floatRight paddingRight5">
                                                <FontAwesomeIcon className="textType" icon={ faUtensils } />
                                            </div>
                        } else if (utensils === "No Utensils") {
                            showUtensils = <div className="floatRight paddingRight5 relativeContainer">
                                                <FontAwesomeIcon className="textNumber noUtensilIcon" icon={ faBan } />
                                                <FontAwesomeIcon className="textType" icon={ faUtensils } />
                                            </div>
                        };
                    };

                    // iterate through item list
                    let orderItems = o["items"];
                    let itemList = orderItems.map((i, n) => {
                        // create unique ids
                        let itemID = ID + n;
                        let idNum = itemID + "num";
                        let idText = itemID + "text";
                        let idMod = itemID + "mod";
                        // extract data
                        let itemName = i["name"];
                        let quantity = i["qty"];
                        let modList = i["mods"];

                        // iterate through modList
                        let itemMods = modList.map((m) => {
                            return (
                                <p className="textMed textLeft">{m}</p>
                            )
                        });

                        return (
                            <div id={itemID} className="flexParent spacing3" onClick={() => crossOut(idNum, idText, idMod)}>
                                <div className="flex1">
                                    <p id={idNum} className="orderNumberBox textNumber textWhite bold">{quantity}</p>
                                </div>
                                <div className="flex5 centerVertical">
                                    <p id={idText} className="textMed textLeft bold">{itemName}</p>
                                    <div id={idMod} className="paddingLeft5">
                                        { itemMods }
                                    </div>
                                </div>
                            </div>
                        )
                    });


                    return (
                        <div id={ID} className="orderCard noHighlight">
                            <div className="orderUpper" onDoubleClick={() => POSTdone(order_num, order_id, square_order_id, orderType, source, o)}>
                                <div className="flexParent">
                                    <div className="flex2">
                                        <p className="textNumber textLeft bold spacing1">{orderID}</p>
                                    </div>
                                    
                                    <div className="flex3">
                                        <p className="textName textRight bold">{name != "" ? name : ""}</p>
                                    </div>
                                </div>
                                <div className="flexParent">
                                    <p className="flex1 textTime textLeft">{orderTime}</p>
                                    <p className="flex1 textTime textRight bold">{orderType}</p>
                                </div>
                            </div>

                            <div className="orderLower">
                                <div className="typeBox flexParent" style={{ backgroundColor: boxColor}}>
                                    <div className="flex1">
                                        <p className="textType textLeft bold">{source}</p>
                                    </div>
                                    <div className="flex1">
                                        <p className="textType textRight bold">{pickupTime != "" ? pickupTime : ""}</p>
                                        { showUtensils }
                                    </div>
                                </div>

                                <div className="paddingLeft2">
                                    { itemList }
                                </div>
                            </div>

                        </div>
                    )

                } catch (err) {
                    console.log("ERROR CREATING NEW ORDER ", err);
                    return null;
                };

            } else {
                return;
            };
        };
        
    });

    return (
        <>
            { showError ? errorPopup : <></> }
            { nameHeader }
            <div className="horizontalScrollContainer">
                <div id="orderCardsContainer" className="cardsContainer paddingLeft1 floatLeft">
                    { showOrders }
                </div>
            </div>
        </>
    )
};






// TEST DATA
const test = [
    {
        "order_id": "831aadbe73a84a569295309de2cf5b0f",
        "order_number": "Order #3452", 
        "order_name": "",
        "source": "KIOSK",
        "square_location_id": "L8P84HQCW0GPA",
        "order_time": "2024-03-13T14:16:14.377000-04:00",  
        "pickup_at": "",
        "items": [
            {
                "name": "Curry Chicken Potsticker (S)",
                "qty": 1,
                "mods": []
            },
            {
                "name": "Pork Soup Dumpling",
                "qty": 1,
                "mods": []
            },
            {
                "name": "Beef Noodle Soup",
                "qty": 1,
                "mods": [
                    "Spicy"
                ]
            },
            {
                "name": "Vegetable Spicy Wonton",
                "qty": 1,
                "mods": []
            }
        ],
        "specialInstructions": "No Utensils"
    },
    {
        "order_id": "d7bb26fa-d0d1-427c-8a15-b047858e74b9",
        "order_number": "Order #9227",
        "order_name": "Yang L",
        "source": "DOORDASH",
        "square_location_id": "L3NR33EJXB8M8",
        "order_time": "2024-03-13T15:45:10.912000-04:00",
        "pickup_at": "2024-03-13T16:01:26-04:00",
        "items": [
            {
                "name": "Pork Spicy Wonton",
                "qty": 1,
                "mods": []
            }
        ],
        "specialInstructions": ""
    },
    {
        "order_id": "3e8fe67193d247f6bc5d4f03778689d5",
        "order_number": "Order #3295",
        "order_name": "",
        "source": "KIOSK",
        "square_location_id": "L3NR33EJXB8M8",
        "order_time": "2024-03-13T15:23:28.444000-04:00",
        "pickup_at": "",
        "items": [
            {
                "name": "Pork Potsticker (S)",
                "qty": 1,
                "mods": []
            },
            {
                "name": "Combo: Hot and Sour Soup",
                "qty": 1,
                "mods": []
            },
            {
                "name": "Combo: Seaweed Salad",
                "qty": 1,
                "mods": []
            }
        ],
        "specialInstructions": "Add Utensils"
    }
]