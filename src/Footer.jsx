import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faRightFromBracket, faRepeat, faRotateRight } from "@fortawesome/free-solid-svg-icons";


export default function Footer({ allowSelect, sendLogout, sendRefresh }) {

    // create logout button
    let logoutButton = <Link to="/" onClick={() => sendLogout(false)}>
                            <button className="buttonBlank buttonLogout floatRight">
                                <p className="subtitle">
                                    <span>Logout&ensp;</span>
                                    <span><FontAwesomeIcon icon={faRightFromBracket} /></span>
                                </p>
                            </button>
                        </Link>

    // create store select button
    let storeSelectButton = <Link to="/stores">
                                <button className="buttonBlank buttonLogout floatRight marginRight3">
                                    <p className="subtitle">
                                        <span>Stores&ensp;</span>
                                        <span><FontAwesomeIcon icon={faRepeat} /></span>
                                    </p>
                                </button>
                            </Link>

    // create refresh orders button
    let refreshButton = <button className="buttonBlank buttonLogout floatRight marginRight3" onClick={() => sendRefresh(true)}>
                            <p className="subtitle">
                                <span>Refresh&ensp;</span>
                                <span><FontAwesomeIcon icon={faRotateRight} /></span>
                            </p>
                        </button>


    return (
        <div className="footerContainer">
            <div className="footerTextContainer">
                <div className="floatLeft div40">
                    <p className="title textLeft textBlue">&ensp;Graphen Kitchen Display</p>
                </div>
                <div className="floatRight div60">
                    { logoutButton }
                    { refreshButton }
                    { allowSelect ? storeSelectButton : <></> }
                </div>
            </div>
        </div>
    )
}