import React, { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
// import components
import './App.css';
import Login from "./Login";
import OrderCards from "./OrderCards"
import Footer from "./Footer";
import StoreLocate from "./StoreLocate";
import Websocket from "./Websocket";

export default function App() {
    const [storeID, setStoreID] = useState(undefined);
    const [MODE, setMODE] = useState("");
    const [allOrders, setAllOrders] = useState([]);
    const [orderRM, setOrderRM] = useState({});
    // flag
    const [isLogin, setIsLogin] = useState(false);
    const [storeSelect, setStoreSelect] = useState(false);
    const [isRemove, setIsRemove] = useState(false);
    const [isRefreshed, setIsRefreshed] = useState(false);


    // reset all states when user logs out
    useEffect(() => {
        if(!isLogin) {
            reset();
        };
    }, [isLogin]);

    ////////////////////
    // RESET FUNCTION //
    ////////////////////
    function reset() {
        setStoreID(undefined);
        setMODE("");
        setAllOrders([]);
        setOrderRM({});
        // flag
        setIsLogin(false);
        setStoreSelect(false);
        setIsRemove(false);
        setIsRefreshed(false);
    };


    //////////////////////
    // DATA FROM CHILD //
    /////////////////////
    function fromOrderCards(flag, dict) {
        setIsRemove(flag);
        setOrderRM(dict)
    }

    console.log("IN APP ", allOrders);

    /////////////////
    // COMPONENTS //
    ////////////////
    let loginPage = <Login sendLoggedin={setIsLogin} sendMODE={setMODE} sendStoreID={setStoreID} sendStoreSelect={setStoreSelect} />

    return (
        <BrowserRouter basename={'/kitchen/display/'}>
            <Routes>
                <Route exact path="/" element={ loginPage } />
                <Route exact path="/stores" element={ isLogin ? <StoreLocate mode={MODE} store={storeID} sendStoreID={setStoreID} /> : loginPage } />
                <Route exact path="/orders" element={ isLogin ? <OrderCards mode={MODE} store={storeID} orderData={allOrders} sendRemove={fromOrderCards} /> : loginPage } />
            </Routes>
            { isLogin ? <Footer allowSelect={storeSelect} sendLogout={setIsLogin} sendRefresh={setIsRefreshed} /> : <></> }
            { isLogin && storeID !== undefined ? <Websocket mode={MODE} store={storeID} sendData={setAllOrders} removeFlag={isRemove} sendRemoveFlag={setIsRemove} removeOrder={orderRM} refresh={isRefreshed} sendRefresh={setIsRefreshed} /> : <></> }
        </BrowserRouter>
    )
}